#!/usr/bin/env python3
import sys

import setuptools
import setuptools.command.test


def get_requirements(filename):
    """Read requirements from file."""
    with open(filename, mode="rt", encoding="utf-8") as f:
        for line in f:
            # TODO: Will not work with #egg-info
            requirement = line.partition("#")[0].strip()
            if not requirement:
                continue
            yield requirement


def get_textfile(filename):
    """Get contents from a text file."""
    with open(filename, mode="rt", encoding="utf-8") as f:
        return f.read().lstrip()


def get_packages():
    """List of (sub)packages to install."""
    return setuptools.find_packages(".", include=("dfo_sap_client", "dfo_sap_client.*"))


def run_setup():
    setup_requirements = ["setuptools_scm"]
    test_requirements = list(get_requirements("requirements-test.txt"))
    install_requirements = list(get_requirements("requirements.txt"))

    if {"build_sphinx", "upload_docs"}.intersection(sys.argv):
        setup_requirements.extend(get_requirements("docs/requirements.txt"))
        setup_requirements.extend(install_requirements)

    setuptools.setup(
        name="dfo-sap-client",
        description="Client for the SAP REST API from DFØ",
        long_description=get_textfile("README.md"),
        long_description_content_type="text/markdown",
        url="https://bitbucket.usit.uio.no/projects/BOTTINT/repos/dfo-sap-client",
        author="USIT, University of Oslo",
        author_email="bnt-int@usit.uio.no",
        use_scm_version=True,
        packages=get_packages(),
        package_data={"dfo_sap_client": ["py.typed"]},
        setup_requires=setup_requirements,
        install_requires=install_requirements,
        tests_require=test_requirements,
        classifiers=[
            "Development Status :: 3 - Alpha",
            "Intended Audience :: Developers",
            "Topic :: Software Development :: Libraries",
            "Programming Language :: Python :: 3 :: Only",
            "Programming Language :: Python :: 3.8",
            "Programming Language :: Python :: 3.9",
            "Programming Language :: Python :: 3.10",
            "Programming Language :: Python :: 3.11",
            "Programming Language :: Python :: 3.12",
            "Programming Language :: Python :: 3.13",
        ],
        keywords="SAP human resource management api client",
    )


if __name__ == "__main__":
    run_setup()
