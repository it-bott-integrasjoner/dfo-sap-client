class SapError(Exception):
    pass


class UnknownResponse(SapError):
    pass
