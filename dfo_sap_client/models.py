import json
from enum import Enum
from typing import (
    Optional,
    TypeVar,
    List,
    Union,
    Type,
    TYPE_CHECKING,
    Any,
    Dict,
    overload,
)
import logging
import pydantic.generics
from pydantic import Field, validator
from pydantic.datetime_parse import parse_date
from decimal import Decimal

import datetime

NameType = TypeVar("NameType")

logger = logging.getLogger(__name__)


T = TypeVar("T")


@overload
def _force_list(value: List[Optional[T]]) -> List[T]:
    pass


@overload
def _force_list(value: None) -> List[Any]:
    pass


@overload
def _force_list(value: T) -> List[T]:
    pass


def _force_list(value: Union[List[Optional[T]], T, None]) -> List[T]:
    """
    Force single values and null to list and filter out nulls

    TODO: Remove method or this comment when the api stabilizes.
    """

    if value is None:
        return []

    if not isinstance(value, list):
        return [value]

    # Empty lists may look like this !! "leder": [null].
    # Filter out null values
    return list(filter(None, value))


if TYPE_CHECKING:
    SapDate = datetime.date
else:

    class SapDate(datetime.date):
        @classmethod
        def __get_validators__(cls):
            def normalize_date(
                value: Union[str, bytes, datetime.date]
            ) -> datetime.date:
                if value == "0000-00-00" or value == b"0000-00-00":
                    return datetime.date.min
                if value == "9999-99-99" or value == b"9999-99-99":
                    return datetime.date.max
                return parse_date(value)

            yield normalize_date


def _encode_sap_date(x: datetime.date) -> str:
    if x == datetime.date.min:
        return "0000-00-00"
    return x.isoformat()


def to_lower_camel(s: str) -> str:
    first, *others = s.split("_")
    return "".join([first.lower(), *map(str.capitalize, others)])


BaseModel_T = TypeVar("BaseModel_T", bound="BaseModel")


class BaseModel(pydantic.BaseModel):
    @classmethod
    def from_dict(cls: Type[BaseModel_T], data: Dict[str, Any]) -> BaseModel_T:
        return cls(**data)

    @classmethod
    def from_json(cls: Type[BaseModel_T], json_data: str) -> BaseModel_T:
        data = json.loads(json_data)
        return cls.from_dict(data)

    @classmethod
    def to_model_class_with_optional_fields(
        cls: Type[BaseModel_T],
    ) -> Type[BaseModel_T]:
        """
        Generate a `BaseModel` class with all the same fields as `cls` but as optional.

        Note: fields of type BaseModel are not changed. In this example the field leder
        is made optional, but leder's fields are not changed.

        >>> class AModel(BaseModel):
        >>>     leder: Leder
        """

        # TODO: Maybe modify fields recursively (if possible) and skip other validators
        # This must be done with care to ensure isinstance(obj, cls) and
        # aliases still work.
        # Pre-validators that massages the value should be run in any case.
        # It would be nice if we could delay validation of fields until they
        # are accessed, but I was unable to implement lazy validation.
        class OptionalModel(cls):  # type: ignore[valid-type,misc]
            ...

        for field in OptionalModel.__fields__.values():
            field.required = False
            field.allow_none = True

        OptionalModel.__name__ = f"Optional{cls.__name__}"

        return OptionalModel

    class Config:
        json_encoders = {
            datetime.date: _encode_sap_date,
        }


class Leder(BaseModel):
    leder_ansattnr: str
    leder_brukerident: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class AuthWf(BaseModel):
    type: str
    bruker: str
    knytning_start: str
    knytning_slutt: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class AnnenId(BaseModel):
    id_type: Optional[str]
    id_beskrivelse: Optional[str]
    id_nr: Optional[str]
    id_startdato: Optional[SapDate]
    id_sluttdato: Optional[SapDate]
    id_land: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Orgenhet(BaseModel):
    id: int
    org_kortnavn: str
    organisasjonsnavn: str
    leder: List[Leder] = []
    pdo: Optional[str]
    type: Optional[str]
    lokasjon_id: Optional[str]
    org_kostnadssted: Optional[str]
    auth_wf: List[AuthWf] = []
    overordn_orgenhet_id: Optional[int]

    class Config:
        fields = {
            "auth_wf": {
                "alias": "autWf",
            },
            "pdo": {
                "alias": "PDO",
            },
        }
        alias_generator = to_lower_camel
        allow_population_by_field_name = True

    @validator("leder", "auth_wf", pre=True)
    def force_list(cls, value: Any) -> List[Any]:
        return _force_list(value)


class Tilleggsstilling(BaseModel):
    startdato: SapDate
    sluttdato: SapDate
    stilling_id: int
    dellonnsprosent: float = Field(..., alias="dellønnsprosent")
    ekstra_stilling: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Ansatt(BaseModel):
    id: str
    brukerident: Optional[str]
    dfo_brukerident: Optional[str]
    ekstern_ident: Optional[str]
    fornavn: str
    etternavn: str
    tittel: Optional[str]
    fnr: Optional[str]
    annen_id: List[AnnenId] = []
    fdato: SapDate
    kjonn: str
    sakarkivnr: Optional[str]  # Ephorte sak, in format sakaar/sakvensnummer
    landkode: str
    medarbeidergruppe: str
    medarbeiderundergruppe: str
    startdato: Optional[SapDate]  # Api doc: Required. Reality: Maybe
    sluttdato: SapDate
    sluttarsak: Optional[str]
    stilling_id: int
    dellonnsprosent: Optional[float] = Field(None, alias="dellønnsprosent")
    bevilgning: Optional[str]
    kostnadssted: Optional[str]
    organisasjon_id: int
    jur_bedriftsnummer: int
    pdo: Optional[str]
    tilleggsstilling: List[Tilleggsstilling] = []
    lederflagg: Optional[bool]
    portaltilgang: Optional[bool]
    turnustilgang: Optional[bool]
    eksternbruker: Optional[bool]
    epost: Optional[str]
    tjenestetelefon: Optional[str]
    privat_telefonnummer: Optional[str]
    telefonnummer: Optional[str]
    mobilnummer: Optional[str]
    mobil_privat: Optional[str]
    privat_tlf_utland: Optional[str]
    privat_postadresse: Optional[str]
    privat_postnr: Optional[str]
    privat_poststed: Optional[str]
    endret_dato: Optional[SapDate]
    endret_av: Optional[str]
    reservasjon_publisering: Optional[bool]
    hjemmel_kode: Optional[str]
    hjemmel_tekst: Optional[str]
    endret_infotype: Optional[str]
    endret_klokkeslett: Optional[datetime.time]
    privat_epost: Optional[str]
    telefon_privat: Optional[str]
    telefon_jobb: Optional[str]

    @property
    def feide_id(self) -> Optional[str]:
        return None if self.ekstern_ident is None else self.ekstern_ident.lower()

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True

    @validator("tilleggsstilling", "annen_id", pre=True, allow_reuse=True)
    def force_list(cls, value: Any) -> List[Any]:
        return _force_list(value)


class AnsattPatch(BaseModel):
    brukerident: Optional[str]
    epost: Optional[str]
    telefonnummer: Optional[str]
    tjenestetelefon: Optional[str]
    mobilnummer: Optional[str]
    mobil_privat: Optional[str]
    ekstern_ident: Optional[str]

    @validator(
        "telefonnummer",
        "tjenestetelefon",
        "mobilnummer",
        "mobil_privat",
        pre=True,
        allow_reuse=True,
    )
    def check_phone_number(cls, value: Any) -> Any:
        if value is None:
            return "slett"
        return value

    @classmethod
    def from_ansatt(cls: Type[BaseModel_T], ansatt: Ansatt) -> BaseModel_T:
        return cls(**ansatt.dict(exclude_unset=True, exclude_defaults=True))

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True
        extra = pydantic.Extra.ignore
        validate_assignment = True


class SlektStr(str, Enum):
    ektefelle = "Ektefelle"
    fraskilt_ektefelle = "Fraskilt ektefelle"
    far = "Far"
    mor = "Mor"
    sep_ektefelle = "Separert ektefelle"
    bror_sos = "Bror/søster"
    reg_partner = "Registrert partner"
    barn = "Barn"
    foreldre = "Foreldre/foresatte"
    annet = "Annet"
    sarkullsbarn = "Særkullsbarn"
    samboer = "Samboer"


class Familie(BaseModel):
    id: str
    slektskap: str  # This is a number, relating to slektskap_tekst
    slektskap_tekst: Optional[str]
    etternavn: Optional[str]
    fornavn: Optional[str]
    fodselsdato: Optional[SapDate]
    nasjonalitet: Optional[str]
    tlf1: Optional[str]
    tlf2: Optional[str]
    kontaktpers_ice: Optional[bool] = Field(None, alias="kontaktpersICE")
    narmeste_familie: Optional[bool]
    startdato: SapDate
    sluttdato: SapDate
    endret_dato: SapDate
    endret_av: Optional[str]
    lopenr: str

    @validator("kontaktpers_ice", "narmeste_familie", pre=True, allow_reuse=True)
    def yes_no(cls, value: Any) -> bool:
        legal_values = ("ja", "nei", "true", "false")
        if value in ("ja", "true") or value is True:
            return True
        elif value in ("nei", "false") or value is False:
            return False
        raise ValueError(f"Invalid value '{value}'. Valid values {legal_values}")

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Innehaver(BaseModel):
    innehaver_ansattnr: str
    innehaver_startdato: Optional[SapDate]
    innehaver_sluttdato: SapDate

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class StillingsKategori(BaseModel):
    stillingskat_id: int
    stillingskat_startdato: SapDate
    stillingskat_sluttdato: SapDate
    stillingskat_betegn: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Stilling(BaseModel):
    id: int
    stillingsnavn: Optional[str]
    stillingskode: Optional[int]
    stillingstittel: Optional[str]
    organisasjon_id: Optional[int]
    yrkeskode: Optional[str]
    yrkeskodetekst: Optional[str]
    innehaver: List[Innehaver] = []
    stillingskat: List[StillingsKategori] = []

    class Config:
        fields = {
            "stillingstittel": {
                "alias": "stillingsgruppe",
            },
        }
        allow_population_by_field_name = True
        alias_generator = to_lower_camel

    @validator("innehaver", "stillingskat", pre=True)
    def force_list(cls, value: Any) -> List[Any]:
        return _force_list(value)


class Kursinformasjon_detalj(BaseModel):
    id: str
    brukerident: Optional[str] = None
    type: str
    spesifisert_utd: str
    kursbetegnelse: Optional[str]
    gyldig_fra_dato: SapDate
    gyldig_til_dato: SapDate
    endret_dato: Optional[SapDate]
    endret_av: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Kursinformasjon(BaseModel):
    kursskjema: List[Kursinformasjon_detalj] = []


class KontraktPerson(BaseModel):
    """
    Model for the result from the ansatteinfokontrakter endpoint. A employee may
    have several contracts with same sekvensnr. 'overlapp = false' indicates
    that its a 'hoved stilling'
    """

    id: str  # Employee id of the contract owner. not the contract itself
    kontraktnr: str
    sekvensnr: str
    type_kontrakt: str
    overlapp: bool  # False: Hoved stilling
    kontraktstype: Optional[str]
    organisasjon_id: int
    kostnadssted: Optional[str]
    kostnadssted_navn: Optional[str]
    stilling_id: Optional[int]
    stillingskode: Optional[int]
    stillingstittel: Optional[str]
    yrkeskode: Optional[str]
    lonnstrinn: Optional[str]
    kronetillegg: Optional[str]
    arslonn: Optional[str]
    dellonnsprosent: Decimal
    timer_fort: Optional[str]
    startdato: SapDate
    sluttdato: SapDate
    endret_dato: Optional[SapDate]
    endret_av: Optional[str]
    saksbehandler: Optional[str]  # in format company-id and intials, for eks: 9900BRAD

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Filtype(str, Enum):
    application_pdf = "application/pdf"


class KontraktFil(BaseModel):
    """
    Model for the result from the infokontrakterFiler endpoint
    """

    sekvensnr: str
    kontraktnr: str
    ansatt_id: str
    status: str
    kostnadssted: Optional[str]
    organisasjon_id: int
    firmakode: Optional[str]
    dato: SapDate
    tidspunkt: str
    filtype: Optional[Filtype]
    filinnhold: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Terminovervakning(BaseModel):
    id: str
    termintype: str
    termintype_tekst: Optional[str]
    avtaledato: Optional[SapDate]
    behandlingsindikator: Optional[str]
    behandlingsindikator_tekst: Optional[str]
    paminnelsesdato: Optional[SapDate]
    tekst1: Optional[str]
    tekst2: Optional[str]
    tekst3: Optional[str]
    endret_dato: Optional[SapDate]
    endret_av: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class AnsattFravar(BaseModel):
    id: str  # ansattnr
    fravarstype: str
    avregningstimer: Optional[str]
    prosent_arbeidsfor: Optional[str]
    startdato: SapDate
    sluttdato: SapDate
    avregningsdager: str
    fravar_lonnet: Optional[bool]
    start_klokkeslett: Optional[str]
    slutt_klokkeslett: Optional[str]
    endret_dato: Optional[SapDate]
    endret_av: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class AnsattPermisjon(BaseModel):
    id: str  # ansattnr
    sekvensnr: Optional[str]
    permisjonstype: Optional[str]
    fullstendig_fravarende: bool
    prosent_arbeidsfor: Optional[str]
    beskrivelse: Optional[str]
    startdato: SapDate
    sluttdato: SapDate
    type_saksbehandling: Optional[str]
    type_saksbehandling_tekst: Optional[str]
    type_saksb_arsak: Optional[str]
    type_saksb_arsak_tekst: Optional[str]
    vedlegg: Optional[bool]
    kommentarliste: Optional[List[Optional[Dict[str, str]]]]
    forste_fravarsdag: Optional[str]
    avregningsdager: Optional[str]
    avregningstimer: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class AnsattePermisjoner(BaseModel):
    ansatt_permisjon: List[AnsattPermisjon] = []

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True

    @validator("ansatt_permisjon", pre=True)
    def force_list(cls, value: Any) -> List[Any]:
        return _force_list(value)


class EskjemaLoggItemLine(BaseModel):
    linjenr: Optional[str]
    type_logglinje: Optional[str]
    dato: Optional[SapDate]
    skjema_status: Optional[str]
    klokkeslett: Optional[str]
    brukernavn: Optional[str]
    prosesstype: Optional[str]
    meldingstekst: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class EskjemaLoggItemVedlegg(BaseModel):
    filtype: Optional[str]
    filnavn: Optional[str]
    filinnhold: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class EskjemaLoggItem(BaseModel):
    sekvensnr: str
    ansatt_id: str
    opprettet_dato: SapDate
    skjematype: str
    skjema_beskrivelse: Optional[str]
    status: str
    logg_liste: Optional[List[EskjemaLoggItemLine]]
    vedlegg: Optional[List[EskjemaLoggItemVedlegg]]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True

    @validator("logg_liste", "vedlegg", pre=True)
    def force_list(cls, value: Any) -> List[Any]:
        return _force_list(value)


class EskjemaLogg(BaseModel):
    eskjema_logg: List[EskjemaLoggItem] = []

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True

    @validator("eskjema_logg", pre=True)
    def force_list(cls, value: Any) -> List[Any]:
        return _force_list(value)
