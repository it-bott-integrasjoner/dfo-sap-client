from .client import SapClient
from .version import get_distribution


__all__ = ["SapClient"]
__version__ = get_distribution().version
