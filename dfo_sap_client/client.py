"""Client for connecting to SAPs WebService"""

from __future__ import annotations

import logging
import re
import typing
import urllib.parse
from datetime import date

import pydantic
import requests
from requests import Response
from urllib3.exceptions import HTTPError

from .exceptions import UnknownResponse
from .models import (
    Ansatt,
    Familie,
    KontraktPerson,
    Kursinformasjon,
    Kursinformasjon_detalj,
    Orgenhet,
    Stilling,
    KontraktFil,
    AnsattPatch,
    Terminovervakning,
    AnsattFravar,
    AnsattPermisjon,
    EskjemaLoggItem,
    BaseModel_T,
)

logger = logging.getLogger(__name__)


def merge_dicts(
    *dicts: typing.Optional[typing.Dict[typing.Any, typing.Any]]
) -> typing.Dict[typing.Any, typing.Any]:
    """
    Combine a series of dicts without mutating any of them.

    >>> merge_dicts({'a': 1}, {'b': 2})
    {'a': 1, 'b': 2}
    >>> merge_dicts({'a': 1}, {'a': 2})
    {'a': 2}
    >>> merge_dicts(None, None, None)
    {}
    """
    combined = dict()
    for d in dicts:
        if not d:
            continue
        for k in d:
            combined[k] = d[k]
    return combined


class SapEndpoints:
    """Get endpoints relative to the SAP API URL."""

    def __init__(
        self,
        baseurl: str,
        *,
        ansatt_url: typing.Optional[str] = None,
        orgenhet_url: typing.Optional[str] = None,
        stilling_url: typing.Optional[str] = None,
        kursinfo_url: typing.Optional[str] = None,
        familie_url: typing.Optional[str] = None,
        ansatteinfokontrakter_url: typing.Optional[str] = None,
        infokontrakterfiler_url: typing.Optional[str] = None,
        ansatte_terminovervakning: typing.Optional[str] = None,
        ansatte_fravar_url: typing.Optional[str] = None,
        ansatte_permisjoner_url: typing.Optional[str] = None,
        eskjemalogg_url: typing.Optional[str] = None,
    ) -> None:
        self.baseurl = baseurl
        self.ansatt_url = ansatt_url or "ansatt/"
        self.orgenhet_url = orgenhet_url or "orgenhet/"
        self.stilling_url = stilling_url or "stilling/"
        self.kursinfo_url = kursinfo_url or "kursgjennomfoering/"
        self.familie_url = familie_url or "ansattefamilie-test/"
        self.ansatteinfokontrakter_url = (
            ansatteinfokontrakter_url or "ansatteinfokontrakter/"
        )
        self.infokontrakterfiler_url = infokontrakterfiler_url or "infokontrakterFiler/"
        self.ansatte_terminovervakning = (
            ansatte_terminovervakning or "ansatteTerminovervakning/"
        )
        self.ansatte_fravar_url = ansatte_fravar_url or "ansattefravar/"
        self.ansatte_permisjoner = ansatte_permisjoner_url or "ansattePermisjoner/"
        self.eskjemalogg = eskjemalogg_url or "eskjemaLogg/"

    def __repr__(self) -> str:
        return "{cls.__name__}({url!r})".format(cls=type(self), url=self.baseurl)

    @staticmethod
    def _urljoin(base: str, path: str, ident: typing.Optional[str]) -> str:
        if ident is None:
            ident = ""
        return urllib.parse.urljoin(base, f"{path}/{ident}")

    def get_ansatt(self, ansatt_id: typing.Optional[str] = None) -> str:
        return self._urljoin(self.baseurl, self.ansatt_url, ansatt_id)

    def get_orgenhet(self, org_id: str) -> str:
        return self._urljoin(self.baseurl, self.orgenhet_url, org_id)

    def get_stilling(self, stilling_id: str) -> str:
        return self._urljoin(self.baseurl, self.stilling_url, stilling_id)

    def get_familie(self, familie_id: str) -> str:
        return self._urljoin(self.baseurl, self.familie_url, familie_id)

    def get_kursinfo(self, employee_id: str) -> str:
        return self._urljoin(self.baseurl, self.kursinfo_url, employee_id)

    def get_ansatteinfokontrakter(self, employee_id: typing.Optional[str]) -> str:
        return self._urljoin(self.baseurl, self.ansatteinfokontrakter_url, employee_id)

    def get_infokontrakterfiler(self, sekvensnr: str = "") -> str:
        return self._urljoin(self.baseurl, self.infokontrakterfiler_url, sekvensnr)

    def get_ansatte_terminovervakning(self, employee_id: str = "") -> str:
        return self._urljoin(self.baseurl, self.ansatte_terminovervakning, employee_id)

    def get_ansatte_fravar(self, employee_id: str = "") -> str:
        return self._urljoin(self.baseurl, self.ansatte_fravar_url, employee_id)

    def get_ansatte_permisjoner(self, employee_id: str = "") -> str:
        return self._urljoin(self.baseurl, self.ansatte_permisjoner, employee_id)

    def get_eskjemalogg(self, sekvensnr: str = "") -> str:
        return self._urljoin(self.baseurl, self.eskjemalogg, sekvensnr)


def to_sorted_positive_ints(
    value: int | str | typing.Iterable[int | str] | None,
) -> tuple[int, ...]:
    if value is None:
        return ()
    if isinstance(value, (str, int)):
        return to_sorted_positive_ints((value,))
    return tuple(
        sorted(set(pydantic.parse_obj_as(pydantic.PositiveInt, x) for x in value))
    )


def faux_paginate(
    value: int | str | typing.Iterable[int | str] | None,
) -> typing.Iterator[str]:
    split_ids = to_sorted_positive_ints(value)
    if not split_ids:
        return
    # Add the first request
    yield f"id lt '{split_ids[0]}'"

    # Add the middle requests
    for i in range(len(split_ids) - 1):
        yield f"id ge '{split_ids[i]}' and id lt '{split_ids[i + 1]}'"

    # Add the last request
    yield f"id ge '{split_ids[-1]}'"


class SapClient:
    default_headers = {
        "Accept": "application/json",
    }

    def __init__(
        self,
        urls: typing.Union[SapEndpoints, typing.Dict[str, typing.Any]],
        tokens: typing.Optional[typing.Dict[str, typing.Dict[str, str]]] = None,
        headers: typing.Optional[typing.Dict[str, str]] = None,
        use_sessions: bool = True,
        ignore_required_model_fields: bool = False,
        match_server_header: typing.Union[str, typing.Pattern[str], None, bool] = True,
        stillinger_split_id: typing.Optional[int | typing.Iterable[int]] = None,
        ansatte_split_id: typing.Optional[int | typing.Iterable[int]] = None,
    ):
        """
        SAP API client.

        :param urls: SAP API endpoint configuration
        :param dict tokens: Tokens for the different APIs
        :param dict headers: Append extra headers to all requests
        :param bool use_sessions: Keep HTTP connections alive (default True)
        :param str|re.Pattern|None|bool match_server_header: Verify the
            "Server" HTTP header (default False)
        """
        if isinstance(urls, dict):
            urls = SapEndpoints(**urls)
        elif not isinstance(urls, SapEndpoints):
            raise TypeError(f"Expected SapEndpoints, got {type(urls)}")
        self.urls = urls
        self.tokens: typing.Dict[str, typing.Dict[str, str]] = tokens or {}
        self.headers = merge_dicts(self.default_headers, headers)
        if match_server_header:
            if match_server_header is True:
                match_server_header = "^SAP NetWeaver Application Server"
            if not isinstance(match_server_header, typing.Pattern):
                match_server_header = re.compile(match_server_header)
        self._server_header_regex: typing.Optional[typing.Pattern[str]] = (
            typing.cast(typing.Pattern[str], match_server_header) or None
        )

        # TODO: Remove retry functionality or adjust status_forcelist
        # if and when SAP API stabilizes
        class SapSession:
            def __init__(self) -> None:
                from requests.adapters import HTTPAdapter
                from urllib3.util.retry import Retry

                self.adapter = HTTPAdapter(
                    max_retries=Retry(
                        total=3,
                        backoff_factor=1,
                        status_forcelist={401, 500, 502, 503, 504},
                    )
                )
                if use_sessions:
                    self.session = requests.Session()
                    self.session.mount(urls.baseurl, adapter=self.adapter)  # type: ignore[union-attr]

            def request(
                self,
                method: str,
                url: str,
                headers: typing.Any,  # noqa
                params: typing.Any,
                **kwargs: typing.Any,
            ) -> Response:
                logger.debug(
                    "%s request(use_sessions=%s, url=%s)",
                    self.__class__.__name__,
                    use_sessions,
                    url,
                )

                def go(session: requests.Session) -> Response:
                    return session.request(
                        method=method, url=url, headers=headers, params=params, **kwargs
                    )

                if use_sessions:
                    logger.debug("Reusing session")
                    return go(self.session)

                with requests.Session() as s:
                    logger.debug("Creating session")
                    s.mount(url, self.adapter)
                    return go(s)

        self.session: SapSession = SapSession()
        self.ignore_required_model_fields = ignore_required_model_fields
        self.stillinger_split_id = stillinger_split_id
        self.ansatte_split_id = ansatte_split_id

    def _build_request_headers(
        self, headers: typing.Dict[str, typing.Any]
    ) -> typing.Dict[str, typing.Any]:
        request_headers = {}
        for h in self.headers:
            request_headers[h] = self.headers[h]
        for h in headers:
            request_headers[h] = headers[h]
        return request_headers

    def call(
        self,
        method_name: str,
        url: str,
        headers: typing.Optional[typing.Dict[str, str]] = None,
        params: typing.Optional[typing.Dict[str, str]] = None,
        **kwargs: typing.Any,
    ) -> typing.Any:
        headers = self._build_request_headers(headers or {})
        if params is None:
            params = {}
        logger.debug(
            "Calling %s %s with params=%r",
            method_name,
            urllib.parse.urlparse(url).path,
            params,
        )
        r = self.session.request(
            method_name, url, headers=headers, params=params, **kwargs
        )
        if self._server_header_regex:
            # DFØ SAP API sets the Server HTTP header.  It is less likely we
            # misinterpret the response when we verify this header.  Esp. 404
            # responses are problematic.  If we blindly trust a 404 to mean
            # "Resource not found", a configuration error or an error in the API
            # manager may be misinterpreted as "This person doesn't have any permisjoner"
            value = r.headers.get("server")
            if value is None or not self._server_header_regex.match(value):
                cause = None
                try:
                    r.raise_for_status()
                except HTTPError as exc:
                    cause = exc
                raise UnknownResponse(
                    f"Could not verify HTTP header 'server'. value = {repr(value)}"
                ) from cause
        if r.status_code in (500, 400, 401) or (r.status_code == 404 and not r.content):
            logger.warning("Got HTTP %d: %r for url: %s", r.status_code, r.content, url)
        return r

    def get(self, url: str, **kwargs: typing.Any) -> typing.Any:
        return self.call("GET", url, **kwargs)

    def put(self, url: str, **kwargs: typing.Any) -> typing.Any:
        return self.call("PUT", url, **kwargs)

    def patch(self, url: str, **kwargs: typing.Any) -> typing.Any:
        return self.call("PATCH", url, **kwargs)

    def to_object(
        self, cls: typing.Type[BaseModel_T], data: typing.Dict[str, typing.Any]
    ) -> BaseModel_T:
        try:
            return cls.from_dict(data)
        except ValueError as e:
            # TODO: How should we report this?
            if "id" in cls.__fields__:
                logger.error("Failed to convert data with id %r: %s", data.get("id"), e)
            else:
                logger.error(e)
            if not self.ignore_required_model_fields:
                raise e
            return cls.to_model_class_with_optional_fields().from_dict(data)

    def get_ansatt(
        self, ansatt_id: str, future_date: typing.Optional[date] = None
    ) -> typing.Union[None, Ansatt]:
        url = self.urls.get_ansatt(ansatt_id)
        response = self.get(url, headers=self.mk_headers("ansatt_api", future_date))
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            ansatte = response.json().get("ansatt", [])
            if isinstance(ansatte, list):
                liste = [self.to_object(Ansatt, ansatt) for ansatt in ansatte]
                if len(liste) != 1:
                    raise HTTPError("Expected one ansatt, but got several.")
                return liste[0]

        response.raise_for_status()
        raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")

    def update_ansatt(
        self, employee_id: str, ansatt_patch: AnsattPatch
    ) -> typing.Optional[str]:
        """Patch of Ansatt where all fields are optional"""

        patch_data = ansatt_patch.json(by_alias=True, exclude_unset=True)
        url_patch_ansatt = self.urls.get_ansatt(employee_id)

        token = self.tokens.get("ansatt_api", {})
        headers = {
            "Content-Type": "application/json",
        }
        headers.update(token)

        response = self.patch(url_patch_ansatt, data=patch_data, headers=headers)
        if response.status_code == 400:
            return "Unacceptable update attempt to update one or more protected fields"
        elif response.status_code == 409:
            return "Unacceptable attempt to update a blocked user"
        else:
            return self.mk_response(response)

    def get_ansatt_by_dfo_brukerident(
        self, dfo_brukerident: str
    ) -> typing.Optional[Ansatt]:
        url = self.urls.get_ansatt()
        # TODO How should we escape values?
        # TODO Create a query builder and maybe add query as parameter to get_all_ansatte
        params = {"q": f"dfoBrukerident eq {dfo_brukerident}"}
        response = self.get(url, headers=self.mk_headers("ansatt_api"), params=params)
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            data = response.json()
            ansatte = data.get("ansatt", {})
            if isinstance(ansatte, list):
                all_ansatte = [self.to_object(Ansatt, ansatt) for ansatt in ansatte]
                if len(all_ansatte) == 1:
                    return all_ansatte[0]
                else:
                    raise RuntimeError(
                        f"Expected 1 ansatt object, got a list with {len(all_ansatte)} items"
                    )

        response.raise_for_status()
        raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")

    def get_orgenhet(self, org_id: str) -> typing.Union[None, Orgenhet]:
        url = self.urls.get_orgenhet(org_id)
        response = self.get(url, headers=self.tokens.get("orgenhet_api", None))
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            organisasjoner = response.json().get("organisasjon", [])
            if isinstance(organisasjoner, list):
                org = [self.to_object(Orgenhet, org) for org in organisasjoner]
                if len(org) != 1:
                    raise HTTPError(
                        f"Expected one orgenhet, but got a list with {len(org)} items."
                    )
                return org[0]

        response.raise_for_status()
        raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")

    def get_stilling(self, stilling_id: str) -> typing.Union[None, Stilling]:
        url = self.urls.get_stilling(stilling_id)
        response = self.get(url, headers=self.tokens.get("stilling_api", None))
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            stillinger = response.json().get("stilling", [])
            if isinstance(stillinger, list):
                stilling = [
                    self.to_object(Stilling, stilling) for stilling in stillinger
                ]
                if len(stilling) != 1:
                    raise HTTPError(
                        f"Expected one stilling object, but got a list with {len(stilling)} items."
                    )
                return stilling[0]

        response.raise_for_status()
        raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")

    def get_familie(self, ansatt_id: str) -> typing.List[Familie]:
        url = self.urls.get_familie(ansatt_id)
        response = self.get(url, headers=self.tokens.get("familie_api", None))

        if response.status_code == 404:
            return []
        elif response.status_code == 200:
            data = response.json()
            if not data:
                return []
            data = data.get("AnsattFamilie", [])
            if isinstance(data, list):
                return [self.to_object(Familie, familie) for familie in data]

        response.raise_for_status()
        raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")

    def get_all_ansatte(self) -> typing.List[Ansatt]:
        split_queries = tuple(faux_paginate(self.ansatte_split_id))
        url = self.urls.get_ansatt()
        responses: typing.Iterable[requests.Response]
        if not split_queries:
            responses = [self.get(url, headers=self.tokens.get("ansatt_api", None))]
        else:
            responses = map(
                lambda q: self.get(
                    url,
                    headers=self.tokens.get("ansatt_api", None),
                    params={"q": q},
                ),
                split_queries,
            )

        ansatte = []
        for response in responses:
            response.raise_for_status()
            if response.status_code == 200:
                data = [
                    self.to_object(Ansatt, entity)
                    for entity in response.json().get("ansatt", [])
                ]
                logger.info(f"{len(data)} ansatte fetched")
                for ansatt in data:
                    ansatte.append(ansatt)
            else:
                raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")
        return ansatte

    def get_all_orgenheter(self) -> typing.List[Orgenhet]:
        url = self.urls.get_orgenhet("")
        response = self.get(url, headers=self.tokens.get("orgenhet_api", None))
        if response.status_code == 200:
            data = response.json().get("organisasjon", [])

            # FIXME: remove when api stops returning duplicates and almost-duplicates
            def unique(values: typing.List[typing.Any]) -> typing.List[typing.Any]:
                if not values:
                    return values
                x, *xs = values
                if any((lambda y: y["id"] == x["id"])(y) for y in xs):
                    return unique(xs)
                else:
                    return [x, *unique(xs)]

            data = unique(data)

            return [self.to_object(Orgenhet, enhet) for enhet in data]
        response.raise_for_status()
        raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")

    def get_all_stillinger(self) -> typing.List[Stilling]:
        split_queries = tuple(faux_paginate(self.stillinger_split_id))
        url = self.urls.get_stilling("")
        responses: typing.Iterable[requests.Response]
        if not split_queries:
            responses = [self.get(url, headers=self.tokens.get("stilling_api", None))]
        else:
            responses = map(
                lambda q: self.get(
                    url,
                    headers=self.tokens.get("stilling_api", None),
                    params={"q": q},
                ),
                split_queries,
            )
        stillinger = []
        for response in responses:
            response.raise_for_status()
            if response.status_code == 200:
                data = [
                    self.to_object(Stilling, entity)
                    for entity in response.json().get("stilling", [])
                ]
                logger.info(f"{len(data)} stillinger fetched")
                for stilling in data:
                    stillinger.append(stilling)
            else:
                raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")
        return stillinger

    def put_kursinformasjon(
        self, employee_id: str, kursinfo: Kursinformasjon
    ) -> typing.Optional[str]:
        """PUT kursinfo to the appropriate API endpoint"""

        data = kursinfo.json(by_alias=True)
        url = self.urls.get_kursinfo(employee_id)

        token = self.tokens.get("kursinfo_api", {})
        headers = {
            "Content-Type": "application/json",
        }
        headers.update(token)

        response = self.put(url, data=data, headers=headers)
        return self.mk_response(response)

    def get_kursinformasjon(
        self, employee_id: str, future_date: typing.Optional[date] = None
    ) -> typing.Optional[typing.List[Kursinformasjon_detalj]]:
        """GET kursinfo from the appropriate API endpoint"""
        url = self.urls.get_kursinfo(employee_id)
        response = self.get(url, headers=self.mk_headers("kursinfo_api", future_date))
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            data = response.json().get("kursskjema", [])
            if isinstance(data, list):
                return [Kursinformasjon_detalj(**kursinfo) for kursinfo in data]
            else:
                return [Kursinformasjon_detalj(**data)]
        response.raise_for_status()
        raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")

    def get_ansatteinfokontrakter(
        self,
        employee_id: typing.Optional[str] = None,
        future_date: typing.Optional[date] = None,
    ) -> typing.Optional[typing.List[KontraktPerson]]:
        url = self.urls.get_ansatteinfokontrakter(employee_id)
        response = self.get(
            url, headers=self.mk_headers("ansatteinfokontrakter_api", future_date)
        )
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            data = response.json().get("AnsattInfotak", {})
            if isinstance(data, list):
                return [KontraktPerson(**kontrakt) for kontrakt in data]
            else:
                return [KontraktPerson(**data)]
        response.raise_for_status()
        raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")

    def get_infokontrakterfiler(
        self,
        sekvensnr: str = "",
        date_from: typing.Optional[date] = None,
        date_to: typing.Optional[date] = None,
    ) -> typing.List[KontraktFil]:
        """Given date_from and date_to wil return all contracts having date between the dates
        Only sekvensnr return array with single contract"""
        if not sekvensnr and not date_to and not date_from:
            raise ValueError("Illegal argument, either sekvens nr or date interval")
        url = self.urls.get_infokontrakterfiler(sekvensnr=sekvensnr)

        query_params = {}
        if date_from and date_to:
            if date_to <= date_from:
                raise ValueError(f"Illegal arg from: {date_from}, to: {date_to}")
            str_frm_d = date_from.strftime("%Y%m%d")
            str_to_d = date_to.strftime("%Y%m%d")
            query_params = {"q": f"dato ge {str_frm_d} AND dato le {str_to_d}"}

        response = self.get(
            url, headers=self.mk_headers("infokontrakterfiler_api"), params=query_params
        )
        if response.status_code == 404:
            logger.debug("Got 404")
            return []
        elif response.status_code == 200:
            data = response.json().get("InfotakFilRespons", [])
            if isinstance(data, list):
                return [KontraktFil(**kontrakt_filer) for kontrakt_filer in data]

        response.raise_for_status()
        raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")

    def get_ansatte_terminovervakning(
        self, ansatt_id: str = ""
    ) -> typing.Optional[typing.List[Terminovervakning]]:
        url = self.urls.get_ansatte_terminovervakning(ansatt_id)
        response = self.get(
            url, headers=self.tokens.get("ansatte_terminovervakning_api", None)
        )

        if response.status_code == 200:
            data = response.json()["AnsattTermin"]
            return [self.to_object(Terminovervakning, x) for x in data]

        response.raise_for_status()
        raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")

    def get_ansatte_fravar(
        self,
        ansatt_id: str = "",
        fravarstype: str = "",
        fra_dato: typing.Optional[date] = None,
        future_date: typing.Optional[date] = None,
    ) -> typing.List[AnsattFravar]:
        params = dict()
        if fra_dato:
            str_fra_dato = fra_dato.strftime("%Y%m%d")
            params["q"] = f"endretDato gt {str_fra_dato}"
        if fravarstype:
            if params.get("q"):
                params["q"] += f" AND fravarstype eq {fravarstype}"
            else:
                params["q"] = f"fravarstype eq {fravarstype}"

        url = self.urls.get_ansatte_fravar(ansatt_id)
        response = self.get(
            url,
            headers=self.mk_headers("ansatte_fravar_api", future_date),
            params=params,
        )

        if response.status_code == 200:
            data = response.json()["AnsattFravaer"]
            return [self.to_object(AnsattFravar, x) for x in data]

        response.raise_for_status()
        raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")

    def get_ansatte_permisjoner(
        self,
        ansatt_id: str = "",
        future_date: typing.Optional[date] = None,
    ) -> typing.List[AnsattPermisjon]:
        url = self.urls.get_ansatte_permisjoner(ansatt_id)
        response = self.get(
            url, headers=self.mk_headers("ansatte_permisjoner_api", future_date)
        )

        if response.status_code == 404:
            logger.debug("Got 404")
            return []
        if response.status_code == 200:
            data = response.json()["AnsattPermisjon"]
            return [self.to_object(AnsattPermisjon, x) for x in data]

        response.raise_for_status()
        raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")

    def get_eskjemalogg(
        self,
        sekvensnr: str = "",
        ansattId: typing.Optional[str] = None,
        skjematype: typing.Optional[str] = None,
        status: typing.Optional[str] = None,
    ) -> typing.List[EskjemaLoggItem]:
        if not sekvensnr:
            raise ValueError("Illegal argument, sekvensnr not specified")

        # TODO: Fix queryparams when the api supports them (params are currently not working)
        query_params = {}
        if ansattId:
            query_params["ansattId"] = ansattId
        if skjematype:
            query_params["skjematype"] = skjematype
        if status:
            query_params["status"] = status

        url = self.urls.get_eskjemalogg(sekvensnr)
        response = self.get(
            url, headers=self.tokens.get("eskjemalogg_api", None), params=query_params
        )

        if response.status_code == 200:
            data = response.json()["EskjemaLogg"]
            return [self.to_object(EskjemaLoggItem, x) for x in data]

        response.raise_for_status()
        raise RuntimeError(f"Unexpected HTTP status: {response.status_code}")

    def mk_headers(
        self, token_key: str, future_date: typing.Optional[date] = None
    ) -> typing.Dict[str, str]:
        """Make headers with api-key and date.
        Date header is used to get data that are available in feature in SAP"""
        headers = self.tokens.get(token_key, {})
        if future_date:
            headers["dato"] = future_date.strftime("%Y%m%d")
        return headers

    def mk_response(self, response: Response) -> typing.Optional[str]:
        if response.status_code == 404:
            return None
        elif response.status_code == 200:
            return "Modified successfully"
        elif response.status_code == 201:
            return "Created successfully"
        response.raise_for_status()
        return None


def get_client(config_dict: typing.Dict[str, typing.Any]) -> SapClient:
    """
    Get a SapClient from configuration.
    """
    return SapClient(**config_dict)
