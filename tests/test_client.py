import json
import re
import typing
from datetime import date, datetime
import requests
import pytest
from pydantic import ValidationError

from dfo_sap_client.client import (
    SapClient,
    SapEndpoints,
    to_sorted_positive_ints,
    faux_paginate,
)
from dfo_sap_client import models
from dfo_sap_client.exceptions import UnknownResponse
from dfo_sap_client.models import AnsattPatch, Terminovervakning, Ansatt


@pytest.fixture
def header_name():
    return "X-Test"


@pytest.fixture
def client_cls(header_name):
    class TestClient(SapClient):
        default_headers = {
            header_name: "6a9a32f0-7322-4ef3-bbce-6685a3388e67",
        }

    return TestClient


@pytest.mark.parametrize(
    argnames="server_header", argvalues=(None, "", "Gluten free server")
)
def test_call_match_server_header_set(config, mock_api, server_header):
    config["match_server_header"] = r"^Test server"
    client = SapClient(**config)
    url = "https://localhost/orgenhet/10000002"
    mock_api.get(
        url,
        json="XXX",
        status_code=200,
        headers={k: v for k, v in {"server": server_header}.items() if v is not None},
    )
    with pytest.raises(UnknownResponse):
        client.call("GET", url)


@pytest.mark.parametrize(
    argnames=("match_server_header", "expected_server_header_regex"),
    argvalues=(
        (None, None),
        (False, None),
        ("", None),
        (True, re.compile(r"^SAP NetWeaver Application Server")),
        (re.compile(r"^Bla bla$"), re.compile(r"^Bla bla$")),
    ),
)
def test_init_match_server_header(
    config, match_server_header, expected_server_header_regex
):
    config["match_server_header"] = match_server_header
    client = SapClient(**config)

    assert client._server_header_regex == expected_server_header_regex


def test_init_does_not_mutate_arg(
    client_cls: typing.Type[SapClient], endpoints: SapEndpoints
) -> None:
    headers: typing.Dict[typing.Any, typing.Any] = {}
    client = client_cls(urls=endpoints, headers=headers)
    assert headers is not client.headers
    assert not headers


def test_init_applies_default_headers(
    client_cls: typing.Type[SapClient],
    endpoints: SapEndpoints,
    header_name: str,
) -> None:
    headers: typing.Dict[typing.Any, typing.Any] = {}
    client = client_cls(urls=endpoints, headers=headers)
    assert header_name in client.headers
    assert client.headers[header_name] == client.default_headers[header_name]


def test_init_modify_defaults(client_cls, endpoints, header_name):
    headers = {header_name: "ede37fdd-a2ae-4a96-9d80-110528425ea6"}
    client = client_cls(urls=endpoints, headers=headers)
    # Check that we respect the headers arg, and don't use default_headers
    assert client.headers[header_name] == headers[header_name]
    # Check that we don't do this by mutating default_headers
    assert client.default_headers[header_name] != headers[header_name]


def test_get_organisasjon(requests_mock, client, organisasjon_bott):
    requests_mock.get("https://localhost/orgenhet/10000002", json=organisasjon_bott)
    org = client.get_orgenhet("10000002")
    assert not isinstance(org, list)
    assert org.id == 10000002
    assert org.org_kortnavn == "BOTT"


def test_get_all_orgenheter(mock_api, client, all_orgenheter):
    original_length = 89
    filtered_length = 52

    assert len(all_orgenheter["organisasjon"]) == original_length
    client.urls.orgenhet_url = "org"
    mock_api.get(client.urls.get_orgenhet(""), text=json.dumps(all_orgenheter))

    orgenheter = client.get_all_orgenheter()
    assert all(isinstance(x, models.Orgenhet) for x in orgenheter)
    assert len(orgenheter) == filtered_length

    # only unique elements?
    assert len(set(map(lambda x: x.id, orgenheter))) == filtered_length


def test_get_ansatteinfokontrakter(requests_mock, client, contracts):
    requests_mock.get(
        "https://localhost/ansatteinfokontrakter/00101223", json=contracts
    )
    resp_contracts = client.get_ansatteinfokontrakter("00101223")
    assert isinstance(resp_contracts, list)
    assert len(resp_contracts) == 3


def test_get_infokontrakterfiler(requests_mock, client, contract_file):
    requests_mock.get("https://localhost/infokontrakterfiler/5223", json=contract_file)
    resp_contract = client.get_infokontrakterfiler("5223")
    assert isinstance(resp_contract, list)
    resp_contract = resp_contract[0]
    assert resp_contract.sekvensnr == "5223"
    assert resp_contract.kontraktnr == "00001376"
    assert resp_contract.ansatt_id == "5001234"
    assert resp_contract.status == "520-Signert"


def test_get_infokontrakterfiler_interval_invalid_arg(client):
    date_from = datetime.strptime("2020-02-01", "%Y-%m-%d")
    date_to = datetime.strptime("2020-01-01", "%Y-%m-%d")

    with pytest.raises(ValueError):
        client.get_infokontrakterfiler(
            sekvensnr="3321", date_from=date_from, date_to=date_to
        )


def test_get_infokontrakterfiler_interval(requests_mock, client, contract_files):
    date_from = datetime.strptime("2021-01-01", "%Y-%m-%d").date()
    date_to = datetime.strptime("2021-02-01", "%Y-%m-%d").date()

    requests_mock.get(
        "https://localhost/infokontrakterfiler/?q=dato+ge+20210101+AND+dato+le+20210201",
        json=contract_files,
    )

    client.get_infokontrakterfiler(date_from=date_from, date_to=date_to)


def test_get_ansatt(requests_mock, client, ansatt_00101223):
    requests_mock.get("https://localhost/ansatte/00101223", json=ansatt_00101223)
    ansatt = client.get_ansatt("00101223")
    assert not isinstance(ansatt, list)
    assert ansatt.jur_bedriftsnummer == 912345678
    assert '"dfoBrukerident": null' in ansatt.json(by_alias=True)
    assert isinstance(ansatt.annen_id[0], models.AnnenId)


def test_get_ansatt_not_found(requests_mock, client, ansatt_00101223):
    requests_mock.get(
        "https://localhost/ansatte/00101223", status_code=404, json={"ansatt": []}
    )
    ansatte = client.get_ansatt("00101223")
    assert ansatte is None


def test_get_ansatt_server_error(requests_mock, client, ansatt_00101223):
    requests_mock.get("https://localhost/ansatte/00101223", status_code=500, json=[])
    with pytest.raises(requests.exceptions.HTTPError) as err:
        client.get_ansatt("00101223")

    assert err.value.response is not None
    assert err.value.response.status_code == 500
    assert err.value.response.json() == []


def test_get_ansatt_date_header(requests_mock, client, ansatt_00101223):
    headers = client.mk_headers("ansatt_api", date.today())
    requests_mock.get(
        "https://localhost/ansatte/00101223",
        json=ansatt_00101223,
        request_headers=headers,
    )
    ansatt = client.get_ansatt("00101223", date.today())
    assert ansatt.jur_bedriftsnummer == 912345678
    assert '"dfoBrukerident": null' in ansatt.json(by_alias=True)
    assert isinstance(ansatt.annen_id[0], models.AnnenId)


def test_update_ansatt(requests_mock, client, ansatt_patch):
    requests_mock.patch("https://localhost/ansatte/00101223", json=ansatt_patch)
    ansatt_patch = AnsattPatch.from_dict(ansatt_patch)
    ansatt_patch_resp = client.update_ansatt("00101223", ansatt_patch)
    assert ansatt_patch_resp is not None
    assert ansatt_patch_resp == "Modified successfully"


def test_update_ansatt_error_404(requests_mock, client, ansatt_patch):
    requests_mock.patch(
        "https://localhost/ansatte/00101223",
        status_code=404,
    )
    ansatt_patch = AnsattPatch.from_dict(ansatt_patch)
    response = client.update_ansatt("00101223", ansatt_patch)
    assert not response


def test_update_ansatt_error_529(requests_mock, client, ansatt_patch):
    requests_mock.patch(
        "https://localhost/ansatte/00101223",
        status_code=529,
    )
    ansatt_patch = AnsattPatch.from_dict(ansatt_patch)
    with pytest.raises(requests.exceptions.HTTPError):
        client.update_ansatt("00101223", ansatt_patch)


def test_update_ansatt_blocked_user_error(requests_mock, client, ansatt_patch):
    requests_mock.patch(
        "https://localhost/ansatte/00101223",
        status_code=409,
    )
    ansatt_patch = AnsattPatch.from_dict(ansatt_patch)
    answer = client.update_ansatt("00101223", ansatt_patch)
    assert answer == "Unacceptable attempt to update a blocked user"


def test_update_ansatt_update_brukerid_error(requests_mock, client, ansatt_patch):
    requests_mock.patch(
        "https://localhost/ansatte/00101223",
        status_code=400,
    )
    ansatt_patch = AnsattPatch.from_dict(ansatt_patch)
    answer = client.update_ansatt("00101223", ansatt_patch)
    assert (
        answer == "Unacceptable update attempt to update one or more protected fields"
    )


def test_get_ansatt_by_dfo_brukerident(requests_mock, client, ansatt_00101223):
    requests_mock.get(
        "https://localhost/ansatte/?q=dfoBrukerident+eq+00101223", json=ansatt_00101223
    )
    ansatt = client.get_ansatt_by_dfo_brukerident("00101223")
    assert isinstance(ansatt, models.Ansatt)


def test_get_familie(requests_mock, client, familie_00101223):
    requests_mock.get(
        "https://localhost/ansattefamilie/00101223", json=familie_00101223
    )
    a = client.get_familie("00101223")
    assert a[0].narmeste_familie is True
    assert a[0].kontaktpers_ice is True


def test_object_or_data_do_not_ignore_required_model_fields_raises_exception(
    client: SapClient,
) -> None:
    client.ignore_required_model_fields = False

    class TestModel(models.BaseModel):
        a: str

    with pytest.raises(ValidationError):
        client.to_object(TestModel, {})


def test_object_or_data_ignore_required_model_fields(client: SapClient) -> None:
    client.ignore_required_model_fields = True

    class TestModel(models.BaseModel):
        a: str

    obj = client.to_object(TestModel, {})

    assert isinstance(obj, TestModel)
    assert obj.a is None


def test_object_or_data_ignore_required_model_fields_logs_id(
    caplog, client: SapClient
) -> None:
    caplog.set_level("ERROR", "dfo_sap_client.logger")
    client.ignore_required_model_fields = True

    class TestModel(models.BaseModel):
        id: int
        a: str

    obj = client.to_object(TestModel, {"id": 12})

    assert isinstance(obj, TestModel)
    assert obj.a is None
    assert caplog.messages[0].startswith(
        "Failed to convert data with id 12: 1 validation error for TestModel"
    )


def test_get_kursinformasjon(requests_mock, client, kursinfo_102989):
    requests_mock.get(
        "https://localhost/kursgjennomfoering/102989", json=kursinfo_102989
    )
    future_date = datetime.strptime("99990101", "%Y%m%d").date()
    a = client.get_kursinformasjon(employee_id="102989", future_date=future_date)
    assert len(a) == 9


def test_get_ansatte_terminovervakning(
    requests_mock,
    client,
    terminovervakning,
):
    data = terminovervakning["AnsattTermin"][0]
    ansatt_id = data["id"]
    requests_mock.get(
        f"https://localhost/ansatteTerminovervakning/{ansatt_id}",
        json=terminovervakning,
    )

    xs = client.get_ansatte_terminovervakning(ansatt_id=ansatt_id)

    assert xs == [Terminovervakning(**data)]
    assert all(isinstance(x, Terminovervakning) for x in xs)
    assert requests_mock.called


def test_get_ansatte_terminovervakning_api_key(
    requests_mock,
    client,
):
    headers = {"X-Gravitee-Api-Key": "<ansatte_terminovervakning-api-key>"}
    requests_mock.get(
        "https://localhost/ansatteTerminovervakning/1",
        json={"AnsattTermin": []},
        request_headers=headers,
    )

    client.get_ansatte_terminovervakning(ansatt_id=1)

    assert requests_mock.called


def test_get_ansatte_permisjoner(requests_mock, client, ansattepermisjoner):
    requests_mock.get(
        "https://localhost/ansattePermisjoner/12345", json=ansattepermisjoner
    )
    ansattePerm = client.get_ansatte_permisjoner(ansatt_id="12345")

    assert len(ansattePerm) == 1
    assert ansattePerm[0].id == "05001234"
    assert ansattePerm[0].sekvensnr == "5223"


def test_get_ansatte_permisjoner_notfound(requests_mock, client, ansattepermisjoner):
    requests_mock.get(
        "https://localhost/ansattePermisjoner/12345", status_code=404, json=[]
    )

    ansattePerm = client.get_ansatte_permisjoner(ansatt_id="12345")
    assert len(ansattePerm) == 0


def test_get_ansatte_permisjoner_error(requests_mock, client, ansattepermisjoner):
    requests_mock.get(
        "https://localhost/ansattePermisjoner/12345",
        status_code=500,
    )

    with pytest.raises(requests.exceptions.HTTPError):
        client.get_ansatte_permisjoner(ansatt_id="12345")


def test_get_stilling(requests_mock, client, stilling_30001133):
    requests_mock.get("https://localhost/stilling/30001133", json=stilling_30001133)
    stilling = client.get_stilling("30001133")
    assert not isinstance(stilling, list)
    assert stilling.stillingsnavn == "1065 Konsulent"
    assert stilling.stillingskode == 20001065
    assert stilling.yrkeskode == "4114 105"


def test_get_stilling_not_found(requests_mock, client, stilling_30001133):
    requests_mock.get(
        "https://localhost/stilling/30001133", status_code=404, json={"stilling": []}
    )
    stillinger = client.get_stilling("30001133")
    assert stillinger is None


@pytest.mark.parametrize("split_id", (None, ()))
def test_get_all_stillinger(split_id, requests_mock, client, stilling_30001133):
    requests_mock.get(
        "https://localhost/stilling/", complete_qs=True, json=stilling_30001133
    )
    client.stillinger_split_id = split_id
    stilling = client.get_all_stillinger()
    assert isinstance(stilling, list)
    assert stilling[0].stillingsnavn == "1065 Konsulent"
    assert stilling[0].stillingskode == 20001065
    assert stilling[0].yrkeskode == "4114 105"


@pytest.mark.parametrize("split_id", (10, (10,)))
def test_get_all_stillinger_split_lt(
    split_id, requests_mock, client, stilling_30001133
):
    mock1 = requests_mock.get(
        "https://localhost/stilling/?q=id+lt+'10'",
        complete_qs=True,
        json=stilling_30001133,
    )
    mock2 = requests_mock.get(
        "https://localhost/stilling/?q=id+ge+'10'",
        complete_qs=True,
        json={"stilling": []},
    )
    client.stillinger_split_id = split_id

    stilling = client.get_all_stillinger()

    assert mock1.called
    assert mock2.called
    assert requests_mock.call_count == 2
    assert isinstance(stilling, list)
    assert stilling[0].stillingsnavn == "1065 Konsulent"
    assert stilling[0].stillingskode == 20001065
    assert stilling[0].yrkeskode == "4114 105"


@pytest.mark.parametrize("split_id", (10, (10,)))
def test_get_all_stillinger_split_ge(
    split_id, requests_mock, client, stilling_30001133
):
    mock1 = requests_mock.get(
        "https://localhost/stilling/?q=id+lt+'10'",
        complete_qs=True,
        json={"stilling": []},
    )
    mock2 = requests_mock.get(
        "https://localhost/stilling/?q=id+ge+'10'",
        complete_qs=True,
        json=stilling_30001133,
    )
    client.stillinger_split_id = split_id

    stilling = client.get_all_stillinger()

    assert mock1.called
    assert mock2.called
    assert requests_mock.call_count == 2
    assert isinstance(stilling, list)
    assert stilling[0].stillingsnavn == "1065 Konsulent"
    assert stilling[0].stillingskode == 20001065
    assert stilling[0].yrkeskode == "4114 105"


def test_get_all_stillinger_split_multiple(requests_mock, client, stilling_30001133):
    mock1 = requests_mock.get(
        "https://localhost/stilling/?q=id+lt+'10'",
        complete_qs=True,
        json={"stilling": []},
    )
    mock2 = requests_mock.get(
        "https://localhost/stilling/?q=id+ge+'10'+and+id+lt+'20'",
        complete_qs=True,
        json=stilling_30001133,
    )
    mock3 = requests_mock.get(
        "https://localhost/stilling/?q=id+ge+'20'+and+id+lt+'30'",
        complete_qs=True,
        json=stilling_30001133,
    )
    mock4 = requests_mock.get(
        "https://localhost/stilling/?q=id+ge+'30'",
        complete_qs=True,
        json=stilling_30001133,
    )
    client.stillinger_split_id = (10, 20, 30)

    stilling = client.get_all_stillinger()

    assert mock1.called
    assert mock2.called
    assert mock3.called
    assert mock4.called
    assert requests_mock.call_count == 4
    assert isinstance(stilling, list)
    assert len(stilling) == 3


@pytest.mark.parametrize("split_id", (10, (10,)))
def test_get_all_stillinger_split_does_not_call_second_request_if_the_first_fails(
    split_id,
    requests_mock,
    client,
    stilling_30001133,
):
    mock1 = requests_mock.get(
        "https://localhost/stilling/?q=id+lt+'10'",
        complete_qs=True,
        json={"stilling": []},
        status_code=404,
    )
    mock2 = requests_mock.get(
        "https://localhost/stilling/?q=id+ge+'10'",
        complete_qs=True,
        json=stilling_30001133,
    )
    client.stillinger_split_id = split_id

    with pytest.raises(requests.exceptions.HTTPError, match="404"):
        client.get_all_stillinger()

    assert mock1.called
    assert not mock2.called
    assert requests_mock.call_count == 1


def test_get_all_ansatte(requests_mock, client, ansatt_00101223):
    requests_mock.get(
        "https://localhost/ansatte/", complete_qs=True, json=ansatt_00101223
    )
    client.ansatte_split_id = None
    ansatte = client.get_all_ansatte()
    assert ansatte == [Ansatt(**ansatt_00101223["ansatt"][0])]


@pytest.mark.parametrize("split_id", (10, (10,)))
def test_get_all_ansatte_split_lt(split_id, requests_mock, client, ansatt_00101223):
    mock1 = requests_mock.get(
        "https://localhost/ansatte/?q=id+lt+'10'",
        complete_qs=True,
        json=ansatt_00101223,
    )
    mock2 = requests_mock.get(
        "https://localhost/ansatte/?q=id+ge+'10'",
        complete_qs=True,
        json=ansatt_00101223,
    )
    client.ansatte_split_id = split_id

    ansatte = client.get_all_ansatte()

    assert mock1.called
    assert mock2.called
    assert requests_mock.call_count == 2
    assert ansatte == [Ansatt(**ansatt_00101223["ansatt"][0])] * 2


@pytest.mark.parametrize("split_id", (10, (10,)))
def test_get_all_ansatte_split_gt(split_id, requests_mock, client, ansatt_00101223):
    mock1 = requests_mock.get(
        "https://localhost/ansatte/?q=id+lt+'10'",
        complete_qs=True,
        json=ansatt_00101223,
    )
    mock2 = requests_mock.get(
        "https://localhost/ansatte/?q=id+ge+'10'",
        complete_qs=True,
        json=ansatt_00101223,
    )
    client.ansatte_split_id = split_id

    ansatte = client.get_all_ansatte()

    assert mock1.called
    assert mock2.called
    assert requests_mock.call_count == 2
    assert ansatte == [Ansatt(**ansatt_00101223["ansatt"][0])] * 2


def test_get_all_ansatte_split_multiple(requests_mock, client, ansatt_00101223):
    mock1 = requests_mock.get(
        "https://localhost/ansatte/?q=id+lt+'10'",
        complete_qs=True,
        json={"ansatt": []},
    )
    mock2 = requests_mock.get(
        "https://localhost/ansatte/?q=id+ge+'10'+and+id+lt+'20'",
        complete_qs=True,
        json=ansatt_00101223,
    )
    mock3 = requests_mock.get(
        "https://localhost/ansatte/?q=id+ge+'20'+and+id+lt+'30'",
        complete_qs=True,
        json=ansatt_00101223,
    )
    mock4 = requests_mock.get(
        "https://localhost/ansatte/?q=id+ge+'30'",
        complete_qs=True,
        json=ansatt_00101223,
    )
    client.ansatte_split_id = (10, 20, 30)

    stilling = client.get_all_ansatte()

    assert mock1.called
    assert mock2.called
    assert mock3.called
    assert mock4.called
    assert requests_mock.call_count == 4
    assert isinstance(stilling, list)
    assert len(stilling) == 3


@pytest.mark.parametrize("split_id", (10, (10,)))
def test_get_all_ansatte_split_does_not_call_second_request_if_the_first_fails(
    split_id,
    requests_mock,
    client,
    ansatt_00101223,
):
    mock1 = requests_mock.get(
        "https://localhost/ansatte/?q=id+lt+'10'",
        complete_qs=True,
        json={"ansatt": []},
        status_code=404,
    )
    mock2 = requests_mock.get(
        "https://localhost/ansatte/?q=id+ge+'10'",
        complete_qs=True,
        json=ansatt_00101223,
    )
    client.ansatte_split_id = split_id

    with pytest.raises(requests.exceptions.HTTPError, match="404"):
        client.get_all_ansatte()

    assert mock1.called
    assert not mock2.called
    assert requests_mock.call_count == 1


@pytest.mark.parametrize(
    argnames=("value", "expected"),
    argvalues=(
        (None, ()),
        ("01239", (1239,)),
        (1239, (1239,)),
        (
            ("01239", "2", 10_000, 1),
            (1, 2, 1239, 10_000),
        ),
        ((1, 1, 1), (1,)),
    ),
)
def test_to_sorted_positive_ints(value, expected):
    assert to_sorted_positive_ints(value) == expected


@pytest.mark.parametrize(
    "value",
    (
        "",
        0,
        "0",
        [0, 1],
        ["0", 1],
        -1,
    ),
)
def test_to_sorted_positive_ints_value_error(value):
    with pytest.raises(ValueError):
        to_sorted_positive_ints(value)


@pytest.mark.parametrize(
    argnames=("value", "expected"),
    argvalues=(
        (None, ()),
        (1239, ("id lt '1239'", "id ge '1239'")),
        ((10, 20), ("id lt '10'", "id ge '10' and id lt '20'", "id ge '20'")),
        (
            (20, 10, 30),
            (
                "id lt '10'",
                "id ge '10' and id lt '20'",
                "id ge '20' and id lt '30'",
                "id ge '30'",
            ),
        ),
    ),
)
def test_faux_paginate(value, expected):
    assert tuple(faux_paginate(value)) == expected
