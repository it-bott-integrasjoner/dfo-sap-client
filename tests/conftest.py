import json
import os
from typing import Any

import pytest
import requests_mock
import yaml

from dfo_sap_client.client import SapClient, SapEndpoints


def load_json_file(name: str) -> Any:
    here = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__).rsplit("/", 1)[0])
    )
    with open(os.path.join(here, "tests/fixtures", name)) as f:
        data = json.load(f)
    return data


@pytest.fixture
def baseurl():
    return "https://localhost"


@pytest.fixture
def endpoints(baseurl):
    return SapEndpoints(baseurl)


@pytest.fixture
def custom_endpoints(baseurl):
    return SapEndpoints(
        baseurl,
        ansatt_url="custom/ansatt/",
        orgenhet_url="custom/organisation/",
        stilling_url="custom/position/",
        kursinfo_url="custom/kursgjennomfoering/",
        familie_url="custom/familie/",
        ansatteinfokontrakter_url="custom/ansatteinfokontrakter/",
        ansatte_terminovervakning="custom/ansatteTerminovervakning/",
        ansatte_permisjoner_url="custom/ansattePermisjoner/",
        eskjemalogg_url="custom/eskjemaLogg/",
    )


@pytest.fixture
def endpoints_without_trailing_slash(baseurl):
    return SapEndpoints(
        baseurl,
        ansatt_url="custom/ansatt",
        ansatteinfokontrakter_url="custom/ansatteinfokontrakter",
    )


@pytest.fixture
def config():
    return get_config("example-config.yml")["client"]


@pytest.fixture
def client(baseurl, config):
    return SapClient(**config)


@pytest.fixture
def mock_api(client):
    with requests_mock.Mocker() as m:
        yield m


@pytest.fixture
def ansatt_00101223():
    return load_json_file("ansatt_00101223.json")


@pytest.fixture
def ansatt_patch():
    return load_json_file("ansatt_patch.json")


@pytest.fixture
def familie_00101223():
    return load_json_file("familie_00101223.json")


@pytest.fixture
def familie_without_name():
    return load_json_file("familie_without_name.json")


@pytest.fixture
def org_bad_format():
    return load_json_file("org_bad_formatting.json")


@pytest.fixture
def org_bad_format_2():
    return load_json_file("org_bad_formatting_2.json")


@pytest.fixture
def stilling_30001133():
    return load_json_file("stilling_30001133.json")


@pytest.fixture
def all_orgenheter():
    return load_json_file("all_orgenheter.json")


@pytest.fixture
def organisasjon_bott():
    return load_json_file("organisasjon_bott.json")


@pytest.fixture
def contract():
    return load_json_file("contract.json").get("AnsattInfotak")


@pytest.fixture
def contracts():
    return load_json_file("contracts.json")


@pytest.fixture
def contract_file():
    return load_json_file("contract_file_5001234.json")


@pytest.fixture
def contract_files():
    return load_json_file("contract_files.json")


@pytest.fixture
def kursinfo_102989():
    return load_json_file("kursinfo_102989.json")


def pytest_collection_modifyitems(config, items):
    if config.option.keyword or config.option.markexpr:
        return
    skip_integration = pytest.mark.skip(
        reason='Not running with pytest -m "integration"'
    )
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_integration)


def get_config(filename: str):
    with open(filename, "r") as f:
        content = f.read()
        return yaml.load(content, Loader=yaml.FullLoader)


@pytest.fixture
def sap_client():
    config = get_config("config.yaml")["client"]

    return SapClient(**config)


@pytest.fixture
def terminovervakning():
    return load_json_file("terminovervakning.json")


@pytest.fixture
def ansattepermisjoner():
    return load_json_file("ansattepermisjoner.json")


@pytest.fixture
def eskjemalogg():
    return load_json_file("eskjemalogg.json")


@pytest.fixture
def eskjemalogg_uten_vedlegg():
    return load_json_file("eskjemalogg_uten_vedlegg.json")
