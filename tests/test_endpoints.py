from dfo_sap_client.client import SapEndpoints


def test_init(baseurl):
    endpoints = SapEndpoints(baseurl)
    assert endpoints.baseurl == baseurl


def test_get_ansatt(baseurl, endpoints):
    url = endpoints.get_ansatt("123")
    assert url == baseurl + f"/ansatt/{123}"


def test_put_ansatt(baseurl, endpoints):
    url = endpoints.get_ansatt("123")
    assert url == baseurl + f"/ansatt/{123}"


def test_get_orgenhet(baseurl, endpoints):
    url = endpoints.get_orgenhet("123")
    assert url == baseurl + f"/orgenhet/{123}"


def test_put_kursinfo(baseurl, endpoints):
    url = endpoints.get_kursinfo("123")
    assert url == baseurl + f"/kursgjennomfoering/{123}"


def test_get_familie(baseurl, endpoints):
    url = endpoints.get_familie("123")
    assert url == baseurl + f"/ansattefamilie-test/{123}"


def test_get_stilling(baseurl, endpoints):
    url = endpoints.get_stilling("123")
    assert url == baseurl + f"/stilling/{123}"


def test_custom_get_ansatt(custom_endpoints, baseurl):
    assert custom_endpoints.get_ansatt("123") == baseurl + "/custom/ansatt/123"


def test_custom_get_orgenhet(custom_endpoints, baseurl):
    assert custom_endpoints.get_orgenhet("123") == baseurl + "/custom/organisation/123"


def test_custom_get_stilling(custom_endpoints, baseurl):
    assert custom_endpoints.get_stilling("123") == baseurl + "/custom/position/123"


def test_without_trailing_slash_ansatt(endpoints_without_trailing_slash, baseurl):
    assert (
        endpoints_without_trailing_slash.get_ansatt("123")
        == baseurl + "/custom/ansatt/123"
    )


def test_get_ansatteinfokontrakter(endpoints, baseurl):
    assert (
        endpoints.get_ansatteinfokontrakter("122")
        == baseurl + "/ansatteinfokontrakter/122"
    )


def test_without_trailing_slash_get_ansatteinfokontrakter(custom_endpoints, baseurl):
    assert (
        custom_endpoints.get_ansatteinfokontrakter("123")
        == baseurl + "/custom/ansatteinfokontrakter/123"
    )


def test_custom_get_ansatteinfokontrakter(endpoints_without_trailing_slash, baseurl):
    assert (
        endpoints_without_trailing_slash.get_ansatteinfokontrakter("124")
        == baseurl + "/custom/ansatteinfokontrakter/124"
    )


def test_get_ansatte_terminovervakning(baseurl, endpoints):
    url = endpoints.get_ansatte_terminovervakning("123")
    assert url == baseurl + f"/ansatteTerminovervakning/{123}"


def test_custom_get_ansatte_terminovervakning(custom_endpoints, baseurl):
    assert (
        custom_endpoints.get_ansatte_terminovervakning("123")
        == baseurl + "/custom/ansatteTerminovervakning/123"
    )


def test_get_ansatte_permisjoner(custom_endpoints, baseurl):
    assert (
        custom_endpoints.get_ansatte_permisjoner("123")
        == baseurl + "/custom/ansattePermisjoner/123"
    )


def test_get_eskjemalogg(custom_endpoints, baseurl):
    assert (
        custom_endpoints.get_eskjemalogg("123") == baseurl + "/custom/eskjemaLogg/123"
    )
