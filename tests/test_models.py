from datetime import datetime, date, time
import json
from typing import Any

import pydantic
import pytest
from pydantic import ValidationError

from dfo_sap_client.models import (
    Ansatt,
    AnnenId,
    AuthWf,
    BaseModel,
    Orgenhet,
    Leder,
    Stilling,
    StillingsKategori,
    Innehaver,
    Familie,
    KontraktPerson,
    KontraktFil,
    _force_list,
    AnsattPatch,
    Terminovervakning,
    AnsattPermisjon,
    EskjemaLoggItem,
    SapDate,
    Tilleggsstilling,
)


def filter_none(xs: list) -> filter:
    return filter(lambda x: x is not None, xs)


def str_to_date(date_string):
    if date_string == "0000-00-00":
        return None
    return datetime.strptime(date_string, "%Y-%m-%d").date()


def test_ansatt(ansatt_00101223):
    data = ansatt_00101223["ansatt"][0]
    ansatt = Ansatt.from_dict(data)

    assert ansatt == Ansatt(
        id="00101223",
        brukerident="zzz",
        dfo_brukerident=None,
        ekstern_ident="ZZZ@EXAMPLE.COM",
        fornavn="Olga",
        etternavn="Bøe",
        tittel="Overbuljongterningpakkmesterassistent",
        fnr="01126500200",
        annen_id=[
            AnnenId(
                id_type="02",
                id_beskrivelse="Passnummer",
                id_nr="JKLFJLDSJAFØ",
                id_startdato=date(1965, 12, 1),
                id_sluttdato=date(9999, 12, 31),
                id_land=None,
            )
        ],
        fdato=date(1965, 12, 1),
        kjonn="F",
        sakarkivnr="2022/1000",
        landkode="NO",
        medarbeidergruppe="4",
        medarbeiderundergruppe="01",
        startdato=date(2020, 10, 1),
        sluttdato=date(9999, 12, 31),
        sluttarsak=None,
        stilling_id=30001176,
        dellonnsprosent=100.0,
        bevilgning=None,
        kostnadssted=None,
        organisasjon_id=10000024,
        jur_bedriftsnummer=912345678,
        pdo=None,
        tilleggsstilling=[
            Tilleggsstilling(
                startdato=date(2020, 11, 30),
                sluttdato=date(2021, 11, 30),
                stilling_id=30001177,
                dellonnsprosent=100.0,
                ekstra_stilling=None,
            )
        ],
        lederflagg=False,
        portaltilgang=True,
        turnustilgang=False,
        eksternbruker=False,
        epost="TEST@EXAMPLE.COM",
        tjenestetelefon="+4723456789",
        privat_telefonnummer=None,
        telefonnummer="+4712345678",
        mobilnummer="+4797654321",
        mobil_privat="+4798765432",
        privat_tlf_utland=None,
        telefon_jobb="+47 12345678",
        telefon_privat="+47 23456789",
        privat_postadresse="Trondheimsvei 66",
        privat_postnr="7014",
        privat_poststed="TRONDHEIM",
        endret_dato=date(2020, 3, 17),
        endret_av="3-ANBE",
        reservasjon_publisering=False,
        endret_infotype="0027",
        endret_klokkeslett=time(0, 40, 21),
        hjemmel_kode="90",
        hjemmel_tekst="A§14.9-2bVikar",
        privat_epost="abc@example.com",
    )


def test_ansatt_patch(ansatt_patch):
    obj = AnsattPatch(**ansatt_patch)

    assert obj == AnsattPatch(
        brukerident="zzz",
        epost="TEST@EXAMPLE.COM",
        telefonnummer="+4712345678",
        tjenestetelefon="+4723456789",
        mobilnummer="+4797654321",
        mobil_privat="+4798765432",
        ekstern_ident="ZZZ@EXAMPLE.COM",
    )


def test_ansatt_patch_phone_numbers_initlialized_to_none():
    obj_dict = json.loads(
        AnsattPatch(
            telefonnummer=None,
            tjenestetelefon=None,
            mobilnummer=None,
            mobil_privat=None,
        ).json()
    )

    assert obj_dict["telefonnummer"] == "slett"
    assert obj_dict["tjenestetelefon"] == "slett"
    assert obj_dict["mobilnummer"] == "slett"
    assert obj_dict["mobil_privat"] == "slett"


def test_ansatt_patch_phone_numbers_assigned_to_none():
    obj = AnsattPatch()
    obj.telefonnummer = None
    obj.tjenestetelefon = None
    obj.mobilnummer = None
    obj.mobil_privat = None
    obj_dict = json.loads(obj.json())

    assert obj_dict["telefonnummer"] == "slett"
    assert obj_dict["tjenestetelefon"] == "slett"
    assert obj_dict["mobilnummer"] == "slett"
    assert obj_dict["mobil_privat"] == "slett"


def test_ansatt_patch_empty_phone_numbers():
    obj_dict = json.loads(AnsattPatch().json())

    assert obj_dict["telefonnummer"] is None
    assert obj_dict["tjenestetelefon"] is None
    assert obj_dict["mobilnummer"] is None
    assert obj_dict["mobil_privat"] is None


def test_ansatt_patch_non_empty_phone_numbers():
    obj_dict = json.loads(
        AnsattPatch(
            telefonnummer=1,
            tjenestetelefon="2",
            mobilnummer="3",
            mobil_privat="4",
        ).json()
    )

    assert obj_dict["telefonnummer"] == "1"
    assert obj_dict["tjenestetelefon"] == "2"
    assert obj_dict["mobilnummer"] == "3"
    assert obj_dict["mobil_privat"] == "4"


def test_ansatt_patch_empty_exclude_unset_is_empty():
    obj_dict = json.loads(AnsattPatch().json(exclude_unset=True))

    assert obj_dict == {}


def test_ansatt_patch_from_ansatt(ansatt_00101223):
    ansatt = Ansatt(**ansatt_00101223["ansatt"][0])
    ansatt_patch = AnsattPatch.from_ansatt(ansatt)

    for field in ansatt_patch.__fields__:
        assert getattr(ansatt_patch, field) == getattr(ansatt, field)


def test_ansatt_patch_to_json():
    ansatt_patch = AnsattPatch(brukerident="foo")
    json = ansatt_patch.json(exclude_unset=True)

    assert json == """{"brukerident": "foo"}"""


def test_ansatt_tilleggsstilling_null_list(ansatt_00101223: Any) -> None:
    data = ansatt_00101223["ansatt"][0]
    data["tilleggsstilling"] = [None]
    ansatt = Ansatt.from_dict(data)
    assert ansatt.tilleggsstilling == []


def test_ansatt_tilleggsstilling_null(ansatt_00101223: Any) -> None:
    data = ansatt_00101223["ansatt"][0]
    data["tilleggsstilling"] = None
    ansatt = Ansatt.from_dict(data)
    assert ansatt.tilleggsstilling == []


def test_ansatt_feide_id(ansatt_00101223: Any) -> None:
    data = ansatt_00101223["ansatt"][0]
    ansatt: Ansatt = Ansatt.from_dict(data)
    ansatt.ekstern_ident = "OLGA@BOTT.INT"

    assert ansatt.feide_id == "olga@bott.int"


def test_ansatt_feide_id_none(ansatt_00101223: Any) -> None:
    data = ansatt_00101223["ansatt"][0]
    ansatt: Ansatt = Ansatt.from_dict(data)
    ansatt.ekstern_ident = None

    assert ansatt.feide_id is None


def test_ansatt_feide_id_is_not_serialized(ansatt_00101223: Any) -> None:
    data = ansatt_00101223["ansatt"][0]
    ansatt: Ansatt = Ansatt.from_dict(data)
    ansatt.ekstern_ident = "OLGA@BOTT.INT"

    assert "feide_id" not in ansatt.dict()
    assert ansatt.feide_id is not None


def test_all_orgenheter(all_orgenheter):
    def do_test_orgenhet(enhet):
        data = enhet

        # Stupid API returns values that looks like numbers as numbers
        def value(key):
            return str(data.get(key)) if data.get(key) is not None else None

        org = Orgenhet.from_dict(data)
        assert org.id == data["id"]  # Because id is now int type
        assert org.org_kortnavn == value("orgKortnavn")
        assert org.organisasjonsnavn == data["organisasjonsnavn"]
        leder = [] if data["leder"] is None else data["leder"]
        assert org.leder == list(
            filter_none([Leder.from_dict(i) if i else None for i in leder])
        )
        assert org.pdo == data["pdo"]
        assert org.type == data["type"]
        assert org.lokasjon_id == data.get("lokasjonId")
        assert org.org_kostnadssted == data["orgKostnadssted"]
        auth_wf = [] if data["autWf"] is None else data["autWf"]
        assert org.auth_wf == list(
            filter_none([AuthWf.from_dict(i) if i else None for i in auth_wf])
        )
        assert data["overordnOrgenhetId"] == org.overordn_orgenhet_id

    for o in all_orgenheter["organisasjon"]:
        do_test_orgenhet(o)


def test_bad_format_org(org_bad_format):
    """
    Check that leder and authWf are forced to lists
    """
    org = Orgenhet(**org_bad_format)
    assert type(org.leder) is list
    assert type(org.auth_wf) is list


def test_bad_format_2_org(org_bad_format_2):
    """
    Check that leder and authWf are don't include null values
    """
    org = Orgenhet(**org_bad_format_2)
    assert org.leder == []
    assert org.auth_wf == []


def test_stilling(stilling_30001133):
    data = stilling_30001133["stilling"][0]

    stillinger = stilling_30001133.get("stilling")
    assert isinstance(stillinger, list)
    assert len(stillinger) == 1
    stilling = Stilling.from_dict(stillinger[0])

    assert stilling.id == data["id"]
    assert stilling.stillingsnavn == data["stillingsnavn"]
    assert stilling.stillingskode == data["stillingskode"]
    assert stilling.stillingstittel == data["stillingsgruppe"]
    assert stilling.organisasjon_id == data["organisasjonId"]
    assert stilling.yrkeskode == data["yrkeskode"]
    assert stilling.yrkeskodetekst == data["yrkeskodetekst"]
    assert stilling.innehaver == [Innehaver.from_dict(i) for i in data["innehaver"]]
    sk = data["stillingskat"]
    if isinstance(sk, list):
        assert stilling.stillingskat == [
            StillingsKategori.from_dict(i) for i in data["stillingskat"]
        ]
    elif isinstance(sk, dict):
        assert stilling.stillingskat == [StillingsKategori.from_dict(sk)]

    else:
        assert stilling.stillingskat == sk


def test_familie(familie_00101223):
    data = familie_00101223["AnsattFamilie"][0]
    familie = Familie.from_dict(data)
    assert familie.id == data["id"]
    assert familie.slektskap == data["slektskap"]
    assert familie.slektskap_tekst == data["slektskap_tekst"]
    assert familie.etternavn == data["etternavn"]
    assert familie.fornavn == data["fornavn"]
    assert familie.fodselsdato == str_to_date(data["fodselsdato"])
    assert familie.nasjonalitet == data["nasjonalitet"]
    assert familie.tlf1 == data["tlf1"]
    assert familie.tlf2 == data["tlf2"]

    kp_ice = data["kontaktpersICE"]
    if kp_ice == "ja":
        assert familie.kontaktpers_ice is True
    elif kp_ice == "nei":
        assert familie.kontaktpers_ice is False
    else:
        assert familie.kontaktpers_ice == kp_ice

    nar_fam = data["narmesteFamilie"]
    if nar_fam == "ja":
        assert familie.narmeste_familie is True
    elif nar_fam == "nei":
        assert familie.narmeste_familie is False
    else:
        assert familie.narmeste_familie == nar_fam

    assert familie.startdato == str_to_date(data["startdato"])
    assert familie.sluttdato == str_to_date(data["sluttdato"])
    assert familie.endret_dato == str_to_date(data["endretDato"])
    assert familie.endret_av == data["endretAv"]
    assert familie.lopenr == data["lopenr"]


def test_familie_without_name(familie_without_name):
    """
    Check that a relative without name is accepted as well as
    that one with a name.
    """
    data = familie_without_name["AnsattFamilie"][0]
    familie = Familie.from_dict(data)
    assert familie.id == data["id"]


def test_contract(contract):
    a_contract = KontraktPerson(**contract)
    assert a_contract.kontraktnr == "00001447"
    assert a_contract.sekvensnr == "0000002496"
    assert a_contract.type_kontrakt == "T3"


def test_contract_file(contract_file):
    data = contract_file["InfotakFilRespons"][0]
    obj = KontraktFil(**data)

    assert data["sekvensnr"] == obj.sekvensnr
    assert data["kontraktnr"] == obj.kontraktnr
    assert data["ansattId"] == obj.ansatt_id
    assert data["status"] == obj.status
    assert data["kostnadssted"] == obj.kostnadssted
    assert data["organisasjonId"] == obj.organisasjon_id
    assert data["firmakode"] == obj.firmakode
    assert str_to_date(data["dato"]) == obj.dato
    assert data["tidspunkt"] == obj.tidspunkt
    assert data["filtype"] == obj.filtype
    assert data["filinnhold"] == obj.filinnhold


def test_to_model_class_with_optional_fields() -> None:
    class TestModel(BaseModel):
        a: str

    with pytest.raises(ValidationError):
        TestModel(**{})

    obj = TestModel.to_model_class_with_optional_fields()(**{})

    assert isinstance(obj, TestModel)
    assert obj.a is None


def test__force_list_singleton() -> None:
    assert _force_list("abc") == ["abc"]


def test__force_list_none() -> None:
    assert _force_list(None) == []


def test__force_list_list() -> None:
    assert _force_list(["abc"]) == ["abc"]


def test__force_list_list_of_lists():
    assert _force_list([["abc"], ["def"]]) == [["abc"], ["def"]]


def test_terminovervakning(terminovervakning):
    # Make sure fixture doesn't have any extra fields
    class X(Terminovervakning):
        class Config:
            extra = pydantic.Extra.forbid

    terminovervakning = terminovervakning["AnsattTermin"][0]
    # noinspection Pydantic
    obj = X(**terminovervakning)

    assert json.loads(obj.json(by_alias=True)) == terminovervakning
    # Make sure fixture has all fields defined
    for value in terminovervakning.values():
        assert value is not None


def test_ansattepermisjoner(ansattepermisjoner):
    # Make sure fixture doesn't have any extra fields
    class X(AnsattPermisjon):
        class Config:
            extra = pydantic.Extra.forbid

    ansatte_permisjoner = ansattepermisjoner["AnsattPermisjon"][0]
    # noinspection Pydantic
    X(**ansatte_permisjoner)

    # Make sure fixture has all fields defined
    for value in ansatte_permisjoner.values():
        assert value is not None


def test_ansattepermisjoner2(ansattepermisjoner):
    permisjoner = ansattepermisjoner["AnsattPermisjon"]
    assert len(permisjoner) == 1
    permisjonItems = [AnsattPermisjon.from_dict(i) for i in permisjoner]
    assert len(permisjonItems) == 1
    assert permisjonItems[0].id == "05001234"
    assert permisjonItems[0].sekvensnr == "5223"


def test_eskjemalogg(eskjemalogg):
    # Make sure fixture doesn't have any extra fields
    class X(EskjemaLoggItem):
        class Config:
            extra = pydantic.Extra.forbid

    eskjemaitems = eskjemalogg["EskjemaLogg"][0]
    # noinspection Pydantic
    X(**eskjemaitems)

    # Make sure fixture has all fields defined
    for value in eskjemaitems.values():
        assert value is not None


def test_eskjemalogg2(eskjemalogg):
    eskjema = eskjemalogg["EskjemaLogg"]
    assert len(eskjema) == 1
    eskjemaloggitems = [EskjemaLoggItem.from_dict(i) for i in eskjema]

    assert len(eskjemaloggitems) == 1
    assert eskjemaloggitems[0].sekvensnr == "5223"
    assert eskjemaloggitems[0].skjema_beskrivelse == "Permisjoner"
    assert eskjemaloggitems[0].logg_liste and len(eskjemaloggitems[0].logg_liste) == 1
    assert eskjemaloggitems[0].logg_liste[0].linjenr == "1"
    assert eskjemaloggitems[0].logg_liste[0].dato == date(2020, 10, 5)


def test_eskjemalogg_uten_vedlegg(eskjemalogg_uten_vedlegg):
    eskjema = eskjemalogg_uten_vedlegg["EskjemaLogg"]
    assert len(eskjema) == 1
    eskjemaloggitems = [EskjemaLoggItem.from_dict(i) for i in eskjema]

    assert len(eskjemaloggitems) == 1
    assert eskjemaloggitems[0].sekvensnr == "5223"
    assert eskjemaloggitems[0].skjema_beskrivelse == "Permisjoner"
    # test of _force_list,
    # to check that when the api returns "vedlegg": [null] it is turned into []
    assert eskjemaloggitems[0].vedlegg == []


def test_sap_date_0000_00_00():
    assert pydantic.parse_obj_as(SapDate, "0000-00-00") == date.min


def test_sap_date_9999_99_99():
    assert pydantic.parse_obj_as(SapDate, "9999-99-99") == date.max


def test_base_model_date_to_json() -> None:
    class DateTest(BaseModel):
        a: SapDate
        b: SapDate
        c: SapDate

    obj = DateTest(a="2000-01-31", b="0000-00-00", c="9999-99-99")

    assert obj.a == date(2000, 1, 31)
    assert obj.b == date.min
    assert obj.c == date.max
    assert obj.json() == '{"a": "2000-01-31", "b": "0000-00-00", "c": "9999-12-31"}'
