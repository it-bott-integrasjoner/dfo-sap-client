from datetime import date, datetime

import pytest

from dfo_sap_client.client import SapClient

from dfo_sap_client.models import Kursinformasjon_detalj, Kursinformasjon

ansatt_nr = "00102607"


@pytest.mark.integration
def test_add_course_to_sap(sap_client: SapClient):
    courseraw = {
        "id": ansatt_nr,
        "type": "K",
        "kursbetegnelse": "mac 10",
        "spesifisert_utd": "anything",
        "gyldig_fra_dato": "2020-10-19",
        "gyldig_til_dato": "9999-12-20",
    }

    kurs_detaljer = Kursinformasjon_detalj(**courseraw)
    courses = Kursinformasjon(kursskjema=[kurs_detaljer])

    kursinformasjon = sap_client.put_kursinformasjon(ansatt_nr, courses)

    assert kursinformasjon is not None
    assert kursinformasjon == "Modified successfully"


@pytest.mark.integration
def test_init_add_course_obj(sap_client: SapClient):
    word = Kursinformasjon_detalj(
        id=ansatt_nr,
        type="K",
        kursbetegnelse="office word",
        spesifisert_utd="fritekst felt",
        gyldig_fra_dato="2010-10-11",
        gyldig_til_dato="9999-12-20",
    )
    word_adv = Kursinformasjon_detalj(
        id=ansatt_nr,
        type="K",
        kursbetegnelse="office word advanced",
        spesifisert_utd="fritekst felt",
        gyldig_fra_dato="2012-10-11",
        gyldig_til_dato="9999-12-20",
    )

    courses = Kursinformasjon(kursskjema=[word, word_adv])

    kursinformasjon = sap_client.put_kursinformasjon(ansatt_nr, courses)

    assert kursinformasjon is not None
    assert kursinformasjon == "Modified successfully"


@pytest.mark.integration
def test_familie(sap_client: SapClient):
    families = sap_client.get_familie(ansatt_nr)
    assert families == []


@pytest.mark.integration
def test_ansatt(sap_client: SapClient):
    ansatt = sap_client.get_ansatt(ansatt_id="101238")
    assert ansatt is not None
    assert ansatt.id == "101238"


@pytest.mark.integration
def test_get_saksbehandler(sap_client: SapClient):
    # get all ansatteinfokontrakter
    infokontrakter = sap_client.get_ansatteinfokontrakter()
    data = set()
    print(f"len: {len(infokontrakter)}")  # type: ignore
    for kontrakt in infokontrakter[0:1]:  # type: ignore
        print(f"{kontrakt.sekvensnr}: ", kontrakt.saksbehandler)
        ansatt = sap_client.get_ansatt_by_dfo_brukerident(kontrakt.saksbehandler)  # type: ignore
        assert ansatt
        print("Ansatt", str(ansatt))
        data.add(kontrakt.saksbehandler)

    assert len(data) == 1
    assert data.pop() == "9900KOPA"


@pytest.mark.integration
def test_get_ansatteinfokontrakter(sap_client: SapClient):
    contracts = sap_client.get_ansatteinfokontrakter("00102632")

    assert contracts is not None
    assert len(contracts) == 1


@pytest.mark.integration
def test_get_infokontrakterfiler(sap_client: SapClient):
    infokontrakt_filer = sap_client.get_infokontrakterfiler(sekvensnr="3321")

    assert len(infokontrakt_filer) == 1
    assert infokontrakt_filer[0].sekvensnr == "0000003321"
    assert infokontrakt_filer[0].ansatt_id == "00102607"


@pytest.mark.integration
def test_get_infokontrakterfiler_interval(sap_client: SapClient):
    date_from = datetime.strptime("2021-01-01", "%Y-%m-%d")
    date_to = datetime.strptime("2021-01-15", "%Y-%m-%d")
    infokontrakt_filer = sap_client.get_infokontrakterfiler(
        date_from=date_from, date_to=date_to
    )
    assert len(infokontrakt_filer) == 9


@pytest.mark.integration
def test_get_ansatte_fravar(sap_client: SapClient):
    ansatte_fravar = sap_client.get_ansatte_fravar(
        future_date=date(2022, 8, 1), fra_dato=date(2022, 8, 1), fravarstype="410"
    )
    assert ansatte_fravar
