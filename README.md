# dfo_sap_client

dfo-sap-client is a Python 3.x library for accessing Human Capital Management data
from DFØ-SAP. It's tailored to fetch data from a custom SAP API.

It was forked from the sap_client library used at UiO and rewritten to fit
the SAP run by DFØ.

The last commit from before the fork was 3a464b9dc91d0f93f733338e7f0616ad7a75c96c

## Usage

```python
from dfo_sap_client import SapClient
from dfo_sap_client.models import Kursinformasjon, Kursinformasjon_detalj

c = SapClient('https://example.com',
              ansatt_url='ansatt/',
              orgenhet_url='orgenhet/',
              stilling_url='stilling/',
              kursinfo_url='kursinfo/',
              familie_url='familie/',
              tokens={'ansatt_api': {'X-Gravitee-API-Key': 'c-d-a-b'},
                      'orgenhet_api': {'X-Gravitee-API-Key': 'a-b-c-d'},
                      'stilling_api': {'X-Gravitee-API-Key': 'd-c-b-a'},
                      'kursinfo_api': {'X-Gravitee-API-Key': 'd-c-b-a'},
                      'familie_api': {'X-Gravitee-API-Key': 'd-c-b-a'}})

c.get_ansatt('00100982')
c.get_orgenhet('10000036')

courseraw = Kursinformasjon_detalj(
        id="102101",
        type="K",
        kursbetegnelse="office word",
        spesifisert_utd="fritekst felt",
        gyldig_fra_dato="2010-10-11",
        gyldig_til_dato="9999-12-20",
    )

courses = Kursinformasjon(kursskjema=[courseraw])

kursinformasjon = c.put_kursinformasjon(102101, courses)

#See integrations test for more details
```

## Run tests:

```python
python setup.py test
```


